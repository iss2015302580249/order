package com.xuyisen.web;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xuyisen.domain.Item;
import com.xuyisen.domain.Order;
import com.xuyisen.service.OrderService;

/**
 * 订单控制类
 * @author sen
 *
 */
@RestController
@RequestMapping("orders")
public class OrderController {
    @Autowired
    private OrderService orderService;
    HashMap<String, Object> hashMap=new HashMap<>();
    List<Map<String, Object>>list=new ArrayList<>();
    

	// 获取登录者订单信息
	@RequestMapping("my_order")
	public List<Map<String, Object>> my_order(HttpSession httpSession) {
		return list = orderService.my_order(httpSession);
	}

	// 获取卖家出售订单信息
	@RequestMapping("sell_orders")
	public List<Map<String, Object>> sell_orders(HttpSession httpSession) {
		return list = orderService.sell_orders(httpSession);
	}
	//添加订单
	@RequestMapping("add_order")
	public HashMap<String,Object>add_order(@ModelAttribute()Item item,HttpSession httpSession){
		if(httpSession.getAttribute("LOGIN_INFO")==null){
			hashMap.put("status", "not_login");
			hashMap.put("msg", "未登录！");
			
		}else if(orderService.add_order(item, httpSession)){
			hashMap.put("status", "success");
			hashMap.put("msg", "添加订单成功！");
		}else {
			hashMap.put("status", "failure");
			hashMap.put("msg", "添加订单失败！");
		}
		return hashMap;
	}
	// 支付订单
		@RequestMapping("pay_order")
		public HashMap<String, Object> pay_order(@ModelAttribute() Order order,
				HttpSession httpSession) {
			if (orderService.pay_order(order, httpSession)) {
				hashMap.put("status", "success");
				hashMap.put("msg", "支付成功！");
			} else {
				hashMap.put("status", "no_money");
				hashMap.put("msg", "你的余额不足，订单支付失败！");
			}
			return hashMap;
		}
		
		// 取消订单
		@RequestMapping("del_order")
		public HashMap<String, Object> del_order(@ModelAttribute() Order order) {
			if (orderService.del_order(order)) {
				hashMap.put("status", "success");
				hashMap.put("msg", "删除订单成功！");
			} else {
				hashMap.put("status", "failure");
				hashMap.put("msg", "删除订单失败");
			}
			return hashMap;
		}
		
		// 确认收货
		@RequestMapping("affirm_goods")
		public HashMap<String, Object> affirm_goods(@ModelAttribute() Order order) {
			if (orderService.affirm_goods(order)) {
				hashMap.put("status", "success");
				hashMap.put("msg", "确认收货成功！");
			} else {
				hashMap.put("status", "failure");
				hashMap.put("msg", "确认收货失败");
			}
			return hashMap;
		}
		
		// 发货
		@RequestMapping("send_goods")
		public HashMap<String, Object> send_goods(@ModelAttribute() Order order,HttpSession httpSession ) {
			if (orderService.send_goods(order,httpSession)) {
				hashMap.put("status", "success");
				hashMap.put("msg", "发货成功！");
			} else {
				hashMap.put("status", "failure");
				hashMap.put("msg", "发货失败");
			}
			return hashMap;
		}
	
}
