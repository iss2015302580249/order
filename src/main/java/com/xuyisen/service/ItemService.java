package com.xuyisen.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xuyisen.dao.ItemDao;
import com.xuyisen.domain.Item;
import com.xuyisen.domain.ItemRepository;

/**
 * 商品服务类
 * @author sen
 *
 */
@Service
public class ItemService {
	@Autowired
	ItemDao itemDao=new ItemRepository();
		
		//获取登陆者商品信息
		public List<Map<String, Object>> my_item(HttpSession httpSession) {
			return itemDao.my_item(httpSession);
		}
		//获取首页商品信息
		public List<Map<String, Object>> home_item() {
			return itemDao.home_item();
		}
		//编辑商品信息
		public boolean edit_goods(Item item, HttpSession httpSession) {
			// TODO Auto-generated method stub
			return itemDao.edit_goods(item,httpSession);
		}
		//获取登录者商品信息
		public List<Map<String, Object>> current_goods(int item_id) {
			// TODO Auto-generated method stub
			return itemDao.current_goods(item_id);
		}
		
	
}

