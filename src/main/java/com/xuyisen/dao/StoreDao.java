package com.xuyisen.dao;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * 店铺接口类
 * @author sen
 *
 */
public interface StoreDao {
	//获取登陆者店铺
	public List<Map<String, Object>> my_store(HttpSession httpSession);
}
