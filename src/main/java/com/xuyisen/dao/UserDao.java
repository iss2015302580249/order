package com.xuyisen.dao;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import com.xuyisen.domain.User;


/**
 * 用户接口类
 * @author sen
 *
 */
public interface UserDao {
	//获取头部信息
	public List<Map<String, Object>>header(HttpSession httpSession);
	//校样用户是否存在
	public boolean proof(User user);
	//用户登录
	public boolean login(User user,HttpSession httpSession);
}
